import Vue from 'vue'
import Vuex from 'vuex'
import { moduleSocket } from 'subsdata'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    sdp: moduleSocket
  },
  state: {
    jwt: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWlndWVsIiwicm9sZXMiOlsiYmFzaWMiLCJhZG1pbiJdfQ.v0u6CeUcanlNUBcX_rVDRDY2e6NCXshYZ7gHgsUTDKM"
    
  },
  mutations: {

  },
  actions: {

  }
})
