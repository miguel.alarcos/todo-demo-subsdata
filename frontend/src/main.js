import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { connect } from 'subsdata'

connect('ws://localhost:8888', store)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
