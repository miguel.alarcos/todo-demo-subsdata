import asyncio
import websockets
from rethinkdb import r
from subsdata.sdp import sdp, method, sub, get_connection, check, has_role, is_logged

async def setup():
    connection = await get_connection()

    await r.expr(['todo']).difference(r.db('test').table_list()). \
        for_each(lambda table: r.db('test').table_create(table)) .\
        run(connection)

loop = asyncio.get_event_loop()
loop.run_until_complete(setup())

@method
@has_role('admin')
async def create_todo(user, description):
    check(description, str)
    connection = await get_connection()
    await r.table('todo').insert({"description": description, "done": False}).run(connection)

@method
@is_logged
async def set_done(user, id, value):
    check(id, str)
    check(value, bool)
    connection = await get_connection()
    await r.table('todo').get(id).update({"done": value}).run(connection)

@sub
def todos(user, done):
    check(done, bool)
    return r.table('todo').filter(lambda row: (row['done'] == done))

def main():    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(websockets.serve(sdp, '0.0.0.0', 8888))
    print("Real time server started at port 8888")
    loop.run_forever()
    loop.close()
