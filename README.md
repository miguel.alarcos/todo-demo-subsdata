cd frontend
yarn
yarn serve

cd backend
docker-compose up --build

browser: open at localhost:8080
open two browsers to see how changes happen in both

```python
# server side

@method
@has_role('admin')
async def create_todo(user, description):
    check(description, str)
    connection = await get_connection()
    await r.table('todo').insert({"description": description, "done": False}).run(connection)

@method
@is_logged
async def set_done(user, id, value):
    check(id, str)
    check(value, bool)
    connection = await get_connection()
    await r.table('todo').get(id).update({"done": value}).run(connection)

@sub
def todos(user, done):
    check(done, bool)
    return r.table('todo').filter(lambda row: (row['done'] == done))

```

```javascript
// client side

<template>
  <div v-if="$subsReady">
    <div>
      <input type="text" v-model="description">
      <button @click="create()">Create Todo</button>
    </div>  
    <div>Done: <input type="checkbox" v-model="done"></div>
    <transition-group name="fade" tag="span">
      <span @click="toggle(todo)" v-bind:key="todo.id" v-for="todo in myTodos" class="hand" v-bind:class="{ done: todo.done }">
        {{ todo.description }}
      </span>  
    </transition-group>
  </div>
  <div v-else>
    Loading...
  </div>
</template>

<script>
import { SDP_Mixin, sdpComputed } from 'subsdata'

export default {
  name: 'Todos',
  mixins: [SDP_Mixin],
  props: {
  },
  predicates: {
      // name-of-the-predicate: [name-of-the-subscription (server side), ...args]
    todos: ['todos', 'done']
  },
  data(){
    return {
      done: false,
      description: ""
    }
  },
  computed: {
      // name-of-the-predicate
    todos(){
      return sdpComputed(this, 'done') // predicate todos will be re-run if done changes
    },
    myTodos(){
      return this.$store.state.sdp.subs.todos
    }
  },
  methods: {
    async toggle(todo){
      await this.$rpc('set_done', {id: todo.id, value: !todo.done})
    },
    async create(){
      await this.$rpc('create_todo', {description: this.description})
      this.description = ""
    }
  }
}
</script>

<style lang="scss">
  .done{
    text-decoration:line-through;
  }
  .hand{
    cursor: pointer;
  }
</style>


```